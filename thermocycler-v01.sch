EESchema Schematic File Version 4
LIBS:thermocycler-v01-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L dk_Transistors-FETs-MOSFETs-Single:IRF540NPBF Q?
U 1 1 60BC50C5
P 6100 1700
F 0 "Q?" H 6208 1753 60  0000 L CNN
F 1 "IRF540NPBF" H 6208 1647 60  0000 L CNN
F 2 "digikey-footprints:TO-220-3" H 6300 1900 60  0001 L CNN
F 3 "https://www.infineon.com/dgdl/irf540npbf.pdf?fileId=5546d462533600a4015355e39f0d19a1" H 6300 2000 60  0001 L CNN
F 4 "IRF540NPBF-ND" H 6300 2100 60  0001 L CNN "Digi-Key_PN"
F 5 "IRF540NPBF" H 6300 2200 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 6300 2300 60  0001 L CNN "Category"
F 7 "Transistors - FETs, MOSFETs - Single" H 6300 2400 60  0001 L CNN "Family"
F 8 "https://www.infineon.com/dgdl/irf540npbf.pdf?fileId=5546d462533600a4015355e39f0d19a1" H 6300 2500 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/infineon-technologies/IRF540NPBF/IRF540NPBF-ND/811869" H 6300 2600 60  0001 L CNN "DK_Detail_Page"
F 10 "MOSFET N-CH 100V 33A TO-220AB" H 6300 2700 60  0001 L CNN "Description"
F 11 "Infineon Technologies" H 6300 2800 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6300 2900 60  0001 L CNN "Status"
	1    6100 1700
	1    0    0    -1  
$EndComp
$Comp
L Device:Thermistor TH?
U 1 1 60BC5D1F
P 2425 2400
F 0 "TH?" H 2530 2446 50  0000 L CNN
F 1 "Thermistor" H 2530 2355 50  0000 L CNN
F 2 "" H 2425 2400 50  0001 C CNN
F 3 "~" H 2425 2400 50  0001 C CNN
	1    2425 2400
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J?
U 1 1 60BC665C
P 1300 4200
F 0 "J?" H 1192 3875 50  0000 C CNN
F 1 "FromArduino5V" H 1192 3966 50  0000 C CNN
F 2 "" H 1300 4200 50  0001 C CNN
F 3 "~" H 1300 4200 50  0001 C CNN
	1    1300 4200
	-1   0    0    1   
$EndComp
Text Notes 1550 4075 0    50   ~ 0
5V
Text Notes 1550 4300 0    50   ~ 0
GND
$Comp
L Device:R R?
U 1 1 60BC7854
P 2425 1775
F 0 "R?" H 2495 1821 50  0000 L CNN
F 1 "R" H 2495 1730 50  0000 L CNN
F 2 "" V 2355 1775 50  0001 C CNN
F 3 "~" H 2425 1775 50  0001 C CNN
	1    2425 1775
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J?
U 1 1 60BC7E8B
P 1850 2075
F 0 "J?" H 1742 1850 50  0000 C CNN
F 1 "A0" H 1742 1941 50  0000 C CNN
F 2 "" H 1850 2075 50  0001 C CNN
F 3 "~" H 1850 2075 50  0001 C CNN
	1    1850 2075
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Female J?
U 1 1 60BCAF7C
P 1050 1075
F 0 "J?" H 1000 875 50  0000 C CNN
F 1 "PowerSupply12V" V 1125 925 50  0000 C CNN
F 2 "" H 1050 1075 50  0001 C CNN
F 3 "~" H 1050 1075 50  0001 C CNN
	1    1050 1075
	-1   0    0    1   
$EndComp
Text Notes 1300 925  0    50   ~ 0
12V
Text Notes 1300 1175 0    50   ~ 0
GND
Wire Wire Line
	1250 1075 1575 1075
Wire Wire Line
	2425 1075 2425 1625
Connection ~ 2425 1075
Wire Wire Line
	2050 2075 2425 2075
Wire Wire Line
	2425 1925 2425 2075
Connection ~ 2425 2075
Wire Wire Line
	2425 2075 2425 2200
Wire Wire Line
	2425 4100 2425 2600
$Comp
L Device:R R?
U 1 1 60BCD8B7
P 3300 1800
F 0 "R?" H 3370 1846 50  0000 L CNN
F 1 "R" H 3370 1755 50  0000 L CNN
F 2 "" V 3230 1800 50  0001 C CNN
F 3 "~" H 3300 1800 50  0001 C CNN
	1    3300 1800
	1    0    0    -1  
$EndComp
$Comp
L Device:Thermistor TH?
U 1 1 60BCDB89
P 3300 2425
F 0 "TH?" H 3405 2471 50  0000 L CNN
F 1 "Thermistor" H 3405 2380 50  0000 L CNN
F 2 "" H 3300 2425 50  0001 C CNN
F 3 "~" H 3300 2425 50  0001 C CNN
	1    3300 2425
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J?
U 1 1 60BCDFFE
P 2825 2075
F 0 "J?" H 2717 1850 50  0000 C CNN
F 1 "A1" H 2717 1941 50  0000 C CNN
F 2 "" H 2825 2075 50  0001 C CNN
F 3 "~" H 2825 2075 50  0001 C CNN
	1    2825 2075
	-1   0    0    1   
$EndComp
Wire Wire Line
	3300 1950 3300 2075
Wire Wire Line
	3025 2075 3300 2075
Connection ~ 3300 2075
Wire Wire Line
	3300 2075 3300 2225
Wire Wire Line
	3300 2625 3300 4100
Wire Wire Line
	3300 4100 2925 4100
Connection ~ 2425 4100
Wire Wire Line
	3300 1075 3300 1650
Wire Wire Line
	2425 1075 3300 1075
$Comp
L Device:R R?
U 1 1 60BD1205
P 5375 1800
F 0 "R?" H 5445 1846 50  0000 L CNN
F 1 "R" H 5445 1755 50  0000 L CNN
F 2 "" V 5305 1800 50  0001 C CNN
F 3 "~" H 5375 1800 50  0001 C CNN
	1    5375 1800
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 60BD1933
P 6100 1250
F 0 "R?" H 6170 1296 50  0000 L CNN
F 1 "R" H 6170 1205 50  0000 L CNN
F 2 "" V 6030 1250 50  0001 C CNN
F 3 "~" H 6100 1250 50  0001 C CNN
	1    6100 1250
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x01_Female J?
U 1 1 60BD2039
P 5650 1525
F 0 "J?" H 5542 1300 50  0000 C CNN
F 1 "A1" H 5542 1391 50  0000 C CNN
F 2 "" H 5650 1525 50  0001 C CNN
F 3 "~" H 5650 1525 50  0001 C CNN
	1    5650 1525
	0    -1   -1   0   
$EndComp
$Comp
L pspice:DIODE D?
U 1 1 60BD2D19
P 6425 1250
F 0 "D?" V 6471 1122 50  0000 R CNN
F 1 "DIODE" V 6380 1122 50  0000 R CNN
F 2 "" H 6425 1250 50  0001 C CNN
F 3 "~" H 6425 1250 50  0001 C CNN
	1    6425 1250
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5800 1800 5650 1800
Wire Wire Line
	5650 1725 5650 1800
Connection ~ 5650 1800
Wire Wire Line
	5650 1800 5525 1800
Wire Wire Line
	6100 1900 5175 1900
Wire Wire Line
	5225 1800 5175 1800
Wire Wire Line
	5175 1800 5175 1900
Connection ~ 5175 1900
Wire Wire Line
	6100 1500 6100 1450
Wire Wire Line
	6425 1450 6100 1450
Connection ~ 6100 1450
Wire Wire Line
	6100 1450 6100 1400
Wire Wire Line
	6100 1100 6100 1050
Wire Wire Line
	6425 1050 6100 1050
Connection ~ 6100 1050
Wire Wire Line
	6100 1050 6100 975 
$Comp
L dk_Transistors-FETs-MOSFETs-Single:IRF540NPBF Q?
U 1 1 60BF0D91
P 6100 2850
F 0 "Q?" H 6208 2903 60  0000 L CNN
F 1 "IRF540NPBF" H 6208 2797 60  0000 L CNN
F 2 "digikey-footprints:TO-220-3" H 6300 3050 60  0001 L CNN
F 3 "https://www.infineon.com/dgdl/irf540npbf.pdf?fileId=5546d462533600a4015355e39f0d19a1" H 6300 3150 60  0001 L CNN
F 4 "IRF540NPBF-ND" H 6300 3250 60  0001 L CNN "Digi-Key_PN"
F 5 "IRF540NPBF" H 6300 3350 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 6300 3450 60  0001 L CNN "Category"
F 7 "Transistors - FETs, MOSFETs - Single" H 6300 3550 60  0001 L CNN "Family"
F 8 "https://www.infineon.com/dgdl/irf540npbf.pdf?fileId=5546d462533600a4015355e39f0d19a1" H 6300 3650 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/infineon-technologies/IRF540NPBF/IRF540NPBF-ND/811869" H 6300 3750 60  0001 L CNN "DK_Detail_Page"
F 10 "MOSFET N-CH 100V 33A TO-220AB" H 6300 3850 60  0001 L CNN "Description"
F 11 "Infineon Technologies" H 6300 3950 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6300 4050 60  0001 L CNN "Status"
	1    6100 2850
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 60BF0D97
P 5375 2950
F 0 "R?" H 5445 2996 50  0000 L CNN
F 1 "R" H 5445 2905 50  0000 L CNN
F 2 "" V 5305 2950 50  0001 C CNN
F 3 "~" H 5375 2950 50  0001 C CNN
	1    5375 2950
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 60BF0D9D
P 6100 2400
F 0 "R?" H 6170 2446 50  0000 L CNN
F 1 "Heat1" H 6170 2355 50  0000 L CNN
F 2 "" V 6030 2400 50  0001 C CNN
F 3 "~" H 6100 2400 50  0001 C CNN
	1    6100 2400
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x01_Female J?
U 1 1 60BF0DA3
P 5650 2675
F 0 "J?" H 5542 2450 50  0000 C CNN
F 1 "A1" H 5542 2541 50  0000 C CNN
F 2 "" H 5650 2675 50  0001 C CNN
F 3 "~" H 5650 2675 50  0001 C CNN
	1    5650 2675
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5800 2950 5650 2950
Wire Wire Line
	5650 2875 5650 2950
Connection ~ 5650 2950
Wire Wire Line
	5650 2950 5525 2950
Wire Wire Line
	6100 3050 5175 3050
Wire Wire Line
	5225 2950 5175 2950
Wire Wire Line
	5175 2950 5175 3050
Connection ~ 5175 3050
Wire Wire Line
	5175 3050 5025 3050
$Comp
L dk_Transistors-FETs-MOSFETs-Single:IRF540NPBF Q?
U 1 1 60BF7393
P 6075 4175
F 0 "Q?" H 6183 4228 60  0000 L CNN
F 1 "IRF540NPBF" H 6183 4122 60  0000 L CNN
F 2 "digikey-footprints:TO-220-3" H 6275 4375 60  0001 L CNN
F 3 "https://www.infineon.com/dgdl/irf540npbf.pdf?fileId=5546d462533600a4015355e39f0d19a1" H 6275 4475 60  0001 L CNN
F 4 "IRF540NPBF-ND" H 6275 4575 60  0001 L CNN "Digi-Key_PN"
F 5 "IRF540NPBF" H 6275 4675 60  0001 L CNN "MPN"
F 6 "Discrete Semiconductor Products" H 6275 4775 60  0001 L CNN "Category"
F 7 "Transistors - FETs, MOSFETs - Single" H 6275 4875 60  0001 L CNN "Family"
F 8 "https://www.infineon.com/dgdl/irf540npbf.pdf?fileId=5546d462533600a4015355e39f0d19a1" H 6275 4975 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/infineon-technologies/IRF540NPBF/IRF540NPBF-ND/811869" H 6275 5075 60  0001 L CNN "DK_Detail_Page"
F 10 "MOSFET N-CH 100V 33A TO-220AB" H 6275 5175 60  0001 L CNN "Description"
F 11 "Infineon Technologies" H 6275 5275 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6275 5375 60  0001 L CNN "Status"
	1    6075 4175
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 60BF7399
P 5350 4275
F 0 "R?" H 5420 4321 50  0000 L CNN
F 1 "R" H 5420 4230 50  0000 L CNN
F 2 "" V 5280 4275 50  0001 C CNN
F 3 "~" H 5350 4275 50  0001 C CNN
	1    5350 4275
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 60BF739F
P 6075 3725
F 0 "R?" H 6145 3771 50  0000 L CNN
F 1 "Heat2" H 6145 3680 50  0000 L CNN
F 2 "" V 6005 3725 50  0001 C CNN
F 3 "~" H 6075 3725 50  0001 C CNN
	1    6075 3725
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x01_Female J?
U 1 1 60BF73A5
P 5625 4000
F 0 "J?" H 5517 3775 50  0000 C CNN
F 1 "A1" H 5517 3866 50  0000 C CNN
F 2 "" H 5625 4000 50  0001 C CNN
F 3 "~" H 5625 4000 50  0001 C CNN
	1    5625 4000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5775 4275 5625 4275
Wire Wire Line
	5625 4200 5625 4275
Connection ~ 5625 4275
Wire Wire Line
	5625 4275 5500 4275
Wire Wire Line
	6075 4375 5150 4375
Wire Wire Line
	5200 4275 5150 4275
Wire Wire Line
	5150 4275 5150 4375
Connection ~ 5150 4375
Connection ~ 6100 1500
Wire Wire Line
	6100 1500 6100 1900
Connection ~ 6100 1900
Wire Wire Line
	6100 2125 7125 2125
Wire Wire Line
	7125 2125 7125 975 
Wire Wire Line
	7125 975  6100 975 
Connection ~ 6100 975 
Wire Wire Line
	6075 3450 7125 3450
Wire Wire Line
	7125 3450 7125 2125
Wire Wire Line
	1250 975  1450 975 
Connection ~ 7125 2125
Wire Wire Line
	5000 3050 4550 3050
Wire Wire Line
	4550 3050 4550 4200
Wire Wire Line
	4550 4375 5150 4375
Wire Wire Line
	4550 1900 4550 3050
Wire Wire Line
	4550 1900 5175 1900
Connection ~ 4550 3050
Wire Wire Line
	6100 2125 6100 2250
Wire Wire Line
	6100 2550 6100 2650
Wire Wire Line
	6075 3450 6075 3575
Wire Wire Line
	6075 3875 6075 3975
Wire Wire Line
	1500 4200 3050 4200
Connection ~ 4550 4200
Wire Wire Line
	4550 4200 4550 4375
$Comp
L Connector:Conn_01x02_Female J?
U 1 1 60C08A4F
P 1025 1900
F 0 "J?" H 975 1700 50  0000 C CNN
F 1 "ToArduino12V" V 1100 1750 50  0000 C CNN
F 2 "" H 1025 1900 50  0001 C CNN
F 3 "~" H 1025 1900 50  0001 C CNN
	1    1025 1900
	-1   0    0    1   
$EndComp
Wire Wire Line
	1225 1800 1450 1800
Wire Wire Line
	1450 1800 1450 975 
Wire Wire Line
	1225 1900 1575 1900
Wire Wire Line
	1575 1900 1575 1075
Connection ~ 1575 1075
Wire Wire Line
	1575 1075 2425 1075
Text Notes 1200 1750 0    50   ~ 0
12V
Text Notes 1225 2000 0    50   ~ 0
GND
$Comp
L Device:Rotary_Encoder SW?
U 1 1 60C0BACE
P 9525 2375
F 0 "SW?" H 9755 2421 50  0000 L CNN
F 1 "Rotary_Encoder" H 9755 2330 50  0000 L CNN
F 2 "" H 9375 2535 50  0001 C CNN
F 3 "~" H 9525 2635 50  0001 C CNN
	1    9525 2375
	1    0    0    -1  
$EndComp
Wire Wire Line
	9225 2275 8000 2275
Wire Wire Line
	9200 2375 8125 2375
Wire Wire Line
	8125 2375 8125 4550
Wire Wire Line
	4550 4550 8125 4550
Connection ~ 4550 4375
Wire Wire Line
	4550 4375 4550 4550
Connection ~ 8125 4550
Wire Wire Line
	8125 4550 8125 4850
Wire Wire Line
	9225 2475 8250 2475
$Comp
L Connector:Conn_01x02_Female J?
U 1 1 60C129B7
P 7625 5050
F 0 "J?" H 7575 4800 50  0000 C CNN
F 1 "Encoder" V 7725 4925 50  0000 C CNN
F 2 "" H 7625 5050 50  0001 C CNN
F 3 "~" H 7625 5050 50  0001 C CNN
	1    7625 5050
	-1   0    0    1   
$EndComp
Wire Wire Line
	7825 4950 8000 4950
Wire Wire Line
	8000 2275 8000 4950
Wire Wire Line
	8250 5050 7825 5050
Wire Wire Line
	8250 2475 8250 5050
Text Notes 7600 4925 0    50   ~ 0
D0
Text Notes 7600 5150 0    50   ~ 0
D1
$Comp
L Analog_Switch:ADG417BN U?
U 1 1 60C195A5
P 4225 975
F 0 "U?" H 4225 1150 50  0000 C CNN
F 1 "ADG417BN" H 4225 1241 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 4225 875 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/ADG417.pdf" H 4225 975 50  0001 C CNN
	1    4225 975 
	-1   0    0    1   
$EndComp
Wire Wire Line
	4525 975  6100 975 
Wire Wire Line
	3925 975  1450 975 
Connection ~ 1450 975 
$Comp
L Connector:Conn_01x04_Male J?
U 1 1 60C22654
P 2650 6600
F 0 "J?" H 2550 6850 50  0000 C CNN
F 1 "Arduino" H 2800 6850 50  0000 C CNN
F 2 "" H 2650 6600 50  0001 C CNN
F 3 "~" H 2650 6600 50  0001 C CNN
	1    2650 6600
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female J?
U 1 1 60C28FD4
P 3500 6600
F 0 "J?" H 3528 6576 50  0000 L CNN
F 1 "LCD_I2C" H 3528 6485 50  0000 L CNN
F 2 "" H 3500 6600 50  0001 C CNN
F 3 "~" H 3500 6600 50  0001 C CNN
	1    3500 6600
	1    0    0    -1  
$EndComp
Text Notes 3175 6600 0    39   ~ 0
5V
Text Notes 3175 6500 0    39   ~ 0
GND
Text Notes 3175 6700 0    39   ~ 0
SDA
Text Notes 3175 6800 0    39   ~ 0
SCL
Text Notes 2550 6700 0    39   ~ 0
D2
Text Notes 2550 6800 0    39   ~ 0
D3
Text Notes 2550 6500 0    39   ~ 0
GND
Text Notes 2550 6600 0    39   ~ 0
5V
Wire Wire Line
	3300 6500 3050 6500
Wire Wire Line
	2850 6600 2925 6600
Wire Wire Line
	3300 6700 2850 6700
Wire Wire Line
	2850 6800 3300 6800
Wire Wire Line
	2925 4100 2925 6600
Wire Wire Line
	2425 4100 2925 4100
Connection ~ 2925 4100
Connection ~ 2925 6600
Wire Wire Line
	2925 6600 3300 6600
Wire Wire Line
	3050 4200 3050 6500
Connection ~ 3050 4200
Wire Wire Line
	3050 4200 3900 4200
Connection ~ 3050 6500
Wire Wire Line
	3050 6500 2850 6500
$Comp
L Switch:SW_Push_Dual SW?
U 1 1 60C36198
P 4025 5600
F 0 "SW?" V 3979 5748 50  0000 L CNN
F 1 "SW_Push_Dual" V 4070 5748 50  0000 L CNN
F 2 "" H 4025 5800 50  0001 C CNN
F 3 "~" H 4025 5800 50  0001 C CNN
	1    4025 5600
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x01_Female J?
U 1 1 60C37535
P 3400 5300
F 0 "J?" H 3292 5075 50  0000 C CNN
F 1 "A1" H 3292 5166 50  0000 C CNN
F 2 "" H 3400 5300 50  0001 C CNN
F 3 "~" H 3400 5300 50  0001 C CNN
	1    3400 5300
	-1   0    0    1   
$EndComp
$Comp
L Device:R R?
U 1 1 60C37BC4
P 3900 5025
F 0 "R?" H 3970 5071 50  0000 L CNN
F 1 "R" H 3970 4980 50  0000 L CNN
F 2 "" V 3830 5025 50  0001 C CNN
F 3 "~" H 3900 5025 50  0001 C CNN
	1    3900 5025
	-1   0    0    1   
$EndComp
Wire Wire Line
	2075 4100 2425 4100
Wire Wire Line
	1500 4100 2425 4100
Wire Wire Line
	3300 4100 4350 4100
Wire Wire Line
	4350 4100 4350 6100
Wire Wire Line
	4350 6100 3925 6100
Connection ~ 3300 4100
Wire Wire Line
	4025 5800 3925 5800
Wire Wire Line
	3925 5800 3925 6100
Wire Wire Line
	3825 5800 3925 5800
Connection ~ 3925 5800
Wire Wire Line
	3825 5400 3900 5400
Wire Wire Line
	3900 5175 3900 5300
Connection ~ 3900 5400
Wire Wire Line
	3900 5400 4025 5400
Wire Wire Line
	3600 5300 3900 5300
Connection ~ 3900 5300
Wire Wire Line
	3900 5300 3900 5400
Wire Wire Line
	3900 4875 3900 4200
Connection ~ 3900 4200
Wire Wire Line
	3900 4200 4550 4200
Wire Notes Line
	7475 1975 7475 5350
Wire Notes Line
	7475 5350 10425 5350
Wire Notes Line
	10425 5350 10425 2000
Wire Notes Line
	10425 2000 7475 2000
Wire Notes Line
	7300 850  4725 850 
Wire Notes Line
	4725 850  4725 4850
Wire Notes Line
	4725 4850 7300 4850
Wire Notes Line
	7300 850  7300 5150
Wire Notes Line
	3225 4725 4600 4725
Wire Notes Line
	4600 4725 4600 6200
Wire Notes Line
	4600 6200 3225 6200
Wire Notes Line
	3225 6200 3225 4725
Wire Notes Line
	2300 6300 2300 7100
Wire Notes Line
	4000 7100 4000 6300
Wire Notes Line
	4000 6300 2300 6300
Wire Notes Line
	2300 7100 4000 7100
Wire Notes Line
	3725 1300 1800 1300
Wire Notes Line
	1800 1300 1800 2725
Wire Notes Line
	1800 2725 3725 2725
Wire Notes Line
	3725 1300 3725 2750
Wire Notes Line
	725  700  725  1250
Wire Notes Line
	725  1250 4600 1250
Wire Notes Line
	4600 1250 4600 700 
Wire Notes Line
	4600 700  725  700 
$EndSCHEMATC
